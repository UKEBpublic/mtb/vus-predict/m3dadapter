from flask import Flask
import json
from flask_cors import CORS
from flask import request
from api_data import VariantData, AAReplacements
from flask_swagger_ui import get_swaggerui_blueprint
import requests

# Create the application instance
app = Flask(__name__)
CORS(app)

### Swagger config ###
SWAGGER_URL = '/M3D/v1/doc'
API_URL = '/static/openapi.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Missense3D-API"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

import file_loader
template = file_loader.load_template('template/template.json')


@app.route('/M3D/v1/toDB')
# usage: '/toDB?uniprot=P04637&variant=R175H'
def requestM3D():
	uniprot = request.args.get('uniprot')
	variant = request.args.get('variant')

	# create dictionary to store results
	result_dict = {}

	# adjust variant input to request Missense3D: p.ARG175HIS
	aa_replacements = AAReplacements.getAAReplacements()
	# exchange actual amino acid by three-letter-code
	act_aa = variant[0]
	mutation = variant.replace(act_aa, aa_replacements[act_aa])
	# exchange mutated amino acid by three-letter-code
	mut_aa = mutation[len(mutation)-1]
	mutation = mutation.replace(mut_aa, aa_replacements[mut_aa])
	mutation = "p." + mutation

	# get precomputed database of Missense3D
	variant_data = VariantData.getVariantData()

	# initialise dictionary to store prediction results of Missense3D
	m3d_prediction_dict = {}

	# create result dict with required information
	if uniprot in variant_data.keys():
		protein_list = variant_data[uniprot]
		for j in range(len(protein_list)):
			if protein_list[j]["mutation"] == mutation:
				act_prediction_dict = protein_list[j]
				m3d_prediction_dict["Prediction"] = act_prediction_dict["Missense3D_prediction"]
				m3d_prediction_dict["Score"] = act_prediction_dict["Missense3D_reason"]
				m3d_prediction_dict["Warning"] = ""
				result_dict[variant] = m3d_prediction_dict
				return result_dict
			else:
				result_dict[variant] = {"Warning": "NOT FOUND IN MISSENSE3D-DB"}
	else:
		result_dict[variant] = {"Warning": "NOT FOUND IN MISSENSE3D-DB"}
	return result_dict

@app.route('/M3D/v1/info') 
def getMetadata():
    headers = {'PRIVATE-TOKEN': 'YqQhg-yv_3vG7t9ubDf3'}
    metadata = requests.get('http://gitlab.gwdg.de/api/v4/projects/12008/repository/branches/master', headers=headers).json()
    meta_out = {"title": 'Missense3D Adapter'}
    meta_out["newest_commit_id"] = metadata["commit"]["short_id"]
    meta_out["newest_commit_message"] = metadata["commit"]["title"]
    meta_out["newest_commit_date"] = metadata["commit"]["authored_date"].split("T")[0]
    meta_out["current_commit_id"], meta_out["current_commit_date"] = extractCommitData()
    return meta_out

def extractCommitData():
    with open("/app/commit_data/current_commit.json", "r") as cf:
        curl_output = cf.readlines()
        commit_info = json.loads(curl_output[0])
        return commit_info["commit"]["short_id"], commit_info["commit"]["authored_date"].split("T")[0]

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
	app.run(host='0.0.0.0', port='9008')
