# DATA TO BE SERVED WITH API
import json
import sys, os

M3D_DB_file = "data/MISSENSE3D-DB.json"
AA_repl_file = "data/AA_replacements.txt"

# store MISSENSE3D-DB.json in Python dictionary
class VariantData(object):
	__instance = None
	variant_data = {}
	try:
		file = open(M3D_DB_file, "r")
		variant_data = json.load(file)
		file.close()
	except IOError:
		sys.exit(os.EX_OSFILE)

	def __init__(self):
		VariantData.variant_data = self

	@staticmethod
	def getVariantData():
		if VariantData.variant_data == {}:
			VariantData()
		return VariantData.variant_data

# store exchange from 1-letter aa code to 3-letter aa code in Python dictionary
class AAReplacements(object):
	__instance = None
	AA_replacements = {}
	try:
		with open(AA_repl_file) as replacements:
			for line in replacements:
				cols = line.strip().split(",")
				AA_replacements[cols[0]] = cols[1]
	except IOError:
		sys.exit(os.EX_OSFILE)

	def __init__(self):
		AAReplacements.AA_replacements = self

	@staticmethod
	def getAAReplacements():
		if AAReplacements.AA_replacements == {}:
			AAReplacements()
		return AAReplacements.AA_replacements
