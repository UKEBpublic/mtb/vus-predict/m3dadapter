# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 11:35:41 2021

@author: kevin
"""
import json

def load_template(filepath):
  with open(filepath, 'r') as fr:
    json_file = fr.read()
  return json.loads(json_file)

def read_input_files(filepath):
  with open(filepath, 'r') as fr:
    data = fr.read().strip().split('\n')
  return data