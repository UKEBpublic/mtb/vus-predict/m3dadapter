FROM python:3.6-alpine

RUN     apk update
RUN     apk add --no-cache curl

WORKDIR /app
ADD ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
COPY . /app

RUN mkdir /app/commit_data
RUN curl -X GET -H "PRIVATE-TOKEN: YqQhg-yv_3vG7t9ubDf3" -L "http://gitlab.gwdg.de/api/v4/projects/12008/repository/branches/master" -o /app/commit_data/current_commit.json

CMD ["python", "server.py"]