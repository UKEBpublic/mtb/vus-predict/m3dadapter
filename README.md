# Adapter for Missense3D-DB

## Description

Flask application that stores the database data from the json file in a python
dictionary and can be requested via localhost.

Application also runs within Docker on port 5000. To run this application with Docker,
type in the following commands in your command line:

To create the image m3d_flask (in the current directory):

docker image build -t m3d_flask .

To run the application with the created image:

docker run -p 5000:5000 --name=m3d_adapter --rm m3d_flask

To choose another port to run this application, just change the docker command as follows:

docker run -p [in here goes your desired port]:5000 --name=m3d_adapter --rm m3d_flask

Usage: localhost:5000/toM3D_DB?uniprot=[UniProt-ID]&variant=[protein description
of variant]

Example input: http://127.0.0.1:5000/toM3D_DB?uniprot=P30531&variant=G63S

Example output: {"GLY63SER":
                    {"Annotation":"CLINVAR:NOT_PROVIDED",
                    "Genename":"SLC6A1",
                    "Missense3D_prediction":"Damaging",
                    "Missense3D_reason":"Buried Gly replaced;Buried / exposed switch;Gly in a bend",
                    "mutation":"p.GLY63SER",
                    "mutation_PDB":"p.GLY63SER",
                    "structure_type":"MODEL",
                    "structure_used":"5I71_A"
                    }
                }
